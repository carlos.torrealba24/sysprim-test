<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarcaController;
use App\Http\Controllers\ModeloController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\VehiculoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// rutas de los CRUDS
Route::resource('modelos', ModeloController::class)->middleware('auth');
Route::resource('marcas', MarcaController::class)->middleware('auth');
Route::resource('vehiculos', VehiculoController::class)->middleware('auth');

// rutas de autenticación
Route::get('/register', [RegisterController::class, 'create'])->name('auth.register');
Route::post('/register', [RegisterController::class, 'store'])->name('auth.store');
Route::get('/login', [SessionController::class, 'create'])->name('auth.login');
Route::post('/home', [SessionController::class, 'entrar'])->name('auth.entrar');
Route::get('/logout', [SessionController::class, 'destroy'])->name('auth.destroy')->middleware('auth');

// ruta para filtrar modelos por la marca
Route::get('/brands/list/{brand}', [ModeloController::class, 'lista'])->name('brand.lista')->middleware('auth');