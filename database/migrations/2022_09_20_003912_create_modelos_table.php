<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_brand')->unsigned();
            $table->string('name',30)->nullable();
            $table->string('description',90)->nullable();
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->foreign('id_brand')->references('id')->on('marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
}
