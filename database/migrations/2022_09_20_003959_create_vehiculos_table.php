<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_brand')->unsigned();
            $table->integer('id_model')->unsigned();
            $table->bigInteger('year')->nullable();
            $table->string('placa',15)->unique()->nullable();
            $table->string('color',30)->nullable();
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->foreign('id_brand')->references('id')->on('marcas');
            $table->foreign('id_model')->references('id')->on('modelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
