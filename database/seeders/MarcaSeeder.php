<?php

namespace Database\Seeders;

use App\Models\Marca;
use Illuminate\Database\Seeder;

class MarcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marca::Create([
            'name' => 'Ford',
            'description' => 'Empresa multinacional de origen estadounidense',
        ]);

        Marca::Create([
            'name' => 'Chevrolet',
            'description' => 'Marca de automoviles y camiones de origen estadounidense',
        ]);

        Marca::Create([
            'name' => 'Lamborghini',
            'description' => 'Automoviles Lamborghini tiene su sede en SantAgata Bolognese',
        ]);

        Marca::Create([
            'name' => 'Ferrari',
            'description' => 'Fabricante de automóviles superdeportivos con sede en Maranello',
        ]);


    }
}
