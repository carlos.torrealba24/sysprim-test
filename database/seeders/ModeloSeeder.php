<?php

namespace Database\Seeders;

use App\Models\Modelo;
use Illuminate\Database\Seeder;

class ModeloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modelo::Create([
            'name' => 'Ranger',
            'description' => 'La mejor camioneta para viajar',
            'id_brand' => 1
        ]);

        Modelo::Create([
            'name' => 'Corvette',
            'description' => 'El modelo más rápido de la marca Chevrolet',
            'id_brand' => 2
        ]);

        Modelo::Create([
            'name' => 'Huracan',
            'description' => 'El modelo más rápido de la marca Lamborghini',
            'id_brand' => 3
        ]);

        Modelo::Create([
            'name' => '296 GTB',
            'description' => 'El modelo más rápido de la marca Ferrari',
            'id_brand' => 4
        ]);
    }
}
