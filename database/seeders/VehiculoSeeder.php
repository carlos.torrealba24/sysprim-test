<?php

namespace Database\Seeders;

use App\Models\Vehiculo;
use Illuminate\Database\Seeder;

class VehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehiculo::Create([
            'id_brand' => 1,
            'id_model' => 1,
            'year' => 2022,
            'placa' => 'ABC123',
            'color' => 'Negro'
        ]);
        Vehiculo::Create([
            'id_brand' => 2,
            'id_model' => 2,
            'year' => 2022,
            'placa' => 'DEF456',
            'color' => 'Azul'
        ]);
        Vehiculo::Create([
            'id_brand' => 3,
            'id_model' => 3,
            'year' => 2022,
            'placa' => 'HIJ789',
            'color' => 'Verde'
        ]);
        Vehiculo::Create([
            'id_brand' => 4,
            'id_model' => 4,
            'year' => 2022,
            'placa' => 'LMN012',
            'color' => 'Rojo'
        ]);
    }
}
