<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    use HasFactory;

    protected $table = "marcas";

    protected $fillable = [
        'name',
        'description',
    ];

    public function modelo()
    {
        return $this->hasMany(Modelo::class, 'id_brand');
    }

    public function vehiculo()
    {
        return $this->hasMany(Vehiculo::class, 'id_brand');
    }
}
