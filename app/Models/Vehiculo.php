<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    use HasFactory;

    protected $table = "vehiculos";

    protected $fillable = [
        'id_brand',
        'id_model',
        'year',
        'placa',
        'color',
    ];

    public function modelo() {
        return $this->belongsTo(Modelo::class, 'id_model');        
    }

    public function marca() {
        return $this->belongsTo(Marca::class, 'id_brand');        
    }
}
