<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SessionController extends Controller
{
    public function create(){
        return view('auth.login');
    }
    

    public function entrar(Request $request){
        $email = $request->email;
        $user = User::where('email', '=', $email)->get()->first();
        if(Hash::check($request->password, $user->password)){
            if(auth()->attempt(request(['email', 'password'])) == true){
                return view('home');
            }
        }else{
            return back()->withErrors([
                'message' => 'Correo o password incorrectos'
            ]);
        }
    }

    public function destroy(){
        auth()->logout();
        return redirect()->route('auth.login');
    }
}
