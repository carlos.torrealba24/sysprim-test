<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MarcaController extends Controller
{

    // función para llamar la tabla con todos los registros
    public function index(){
        $marcas = Marca::where('status', '=', 1)->get();
        return view('marcas.index', compact('marcas'));
    }

    // función para llamar el formulario
    public function create(){
        return view('marcas.create');
    }

    // función para guardar un registro nuevo
    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:30',
            'description' => 'required|max:90',
        ]);
        $marcas = new Marca();
        $marcas->name = $request->name;
        $marcas->description = $request->description;
        $marcas->save();
        return response()->json(['success' => true]);
    }

    // función para llamar el formulario con información del registro a editar
    public function edit($id){
        $marcas = Marca::where('id', '=', $id)->get()->first();
        return view('marcas.edit', compact('marcas'));
    }

    // función para modificar un registro
    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required|max:30',
            'description' => 'required|max:90',
        ]);
        $marcas = Marca::where('id', '=', $id)->get()->first();
        $marcas->name = $request->name;
        $marcas->description = $request->description;
        $marcas->save();
        return response()->json(['success' => true]);
    }

    // función para eliminar logicamente un registro
    public function destroy(Request $request, $id){
        $marcas = Marca::where('id', '=', $id)->get()->first();
        $marcas->status = '0';
        $marcas->save();
        return response()->json(['success' => true]);
    }
}
