<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use App\Models\Modelo;
use Illuminate\Http\Request;

class ModeloController extends Controller
{
    // función para llamar la tabla con todos los registros
    public function index(){
        $modelos = Modelo::where('status', '=', 1)->with('marca')->get();
        return view('modelos.index', compact('modelos'));
    }

    // función para llamar el formulario
    public function create(){
        $marcas = Marca::where('status', '=', 1)->get();
        return view('modelos.create', compact('marcas'));
    }

    // función para guardar un registro nuevo
    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:30',
            'description' => 'required|max:90',
        ]);
        $modelos = new Modelo();
        $modelos->name = $request->name;
        $modelos->description = $request->description;
        $modelos->id_brand = $request->id_brand;
        $modelos->save();
        return response()->json(['success' => true]);
    }

    // función para llamar el formulario con información del registro a editar
    public function edit($id){
        $modelos = Modelo::where('id', '=', $id)->with('marca')->get()->first();
        $marcas = Marca::where('status', '=', 1)->get();
        return view('modelos.edit', compact('modelos', 'marcas'));

    }

    // función para modificar un registro
    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required|max:30',
            'description' => 'required|max:90',
        ]);
        $modelos = Modelo::where('id', '=', $id)->get()->first();
        $modelos->name = $request->name;
        $modelos->description = $request->description;
        $modelos->id_brand = $request->id_brand;
        $modelos->save();
        return response()->json(['success' => true]);
    }

    // función para eliminar logicamente un registro
    public function destroy($id){
        $modelos = Modelo::where('id', '=', $id)->get()->first();
        $modelos->status = '0';
        $modelos->save();
        return response()->json(['success' => true]);
    }

    // funcion que retorna una lista de modelos filtrados por la marca
    public function lista($id){
        $modelos = Modelo::where([
            ['status', '=', '1'],
            ['id_brand','=',$id]
        ])->with('marca')->get();

        $modelsNotFound[] = ['id'=> 'null', 'name' => 'Esta marca no tiene modelos de vehiculos asociados'];

        count($modelos) === 0 ? $modelos = $modelsNotFound : $modelos;

        return $modelos;
        
    }
}
