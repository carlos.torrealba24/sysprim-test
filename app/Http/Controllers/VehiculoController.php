<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VehiculoController extends Controller
{

    // función para llamar la tabla con todos los registros
    public function index(){
        $vehiculos = Vehiculo::where('status', '=', 1)->with('modelo')->with('marca')->get();
        return view('vehiculos.index', compact('vehiculos'));
    }

    // función para llamar el formulario
    public function create(){
        $modelos = Modelo::where('status', '=', 1)->with('marca')->get();
        $marcas = Marca::where('status', '=', 1)->get();
        return view('vehiculos.create', compact('modelos', 'marcas'));
    }

    // función para guardar un registro nuevo
    public function store(Request $request){
        $request->validate([
            'id_model' => 'required',
            'id_brand' => 'required',
            'year' => 'required',
            'placa' => 'required|unique:vehiculos|max:15',
            'color' => 'required|max:30'
        ]);
        $vehiculos = new Vehiculo();
        $vehiculos->id_brand = $request->id_brand;
        $vehiculos->id_model = $request->id_model;
        $vehiculos->year = $request->year;
        $vehiculos->placa = $request->placa;
        $vehiculos->color = $request->color;
        $vehiculos->save();
        return response()->json(['success' => true]);
    }

    // función para llamar el formulario con información del registro a editar
    public function edit($id){
        $modelos = Modelo::where('status', '=', 1)->with('marca')->get();
        $marcas = Marca::where('status', '=', 1)->get();
        $vehiculos = Vehiculo::where('id','=', $id)->get()->first();
        return view('vehiculos.edit', compact('modelos', 'marcas', 'vehiculos'));
    }

    // función para modificar un registro
    public function update(Request $request, $id){
        $request->validate([
            'id_model' => 'required',
            'id_brand' => 'required',
            'year' => 'required',
            'placa' => 'required|unique:vehiculos,placa,'.$id.'|max:15',
            'color' => 'required|max:30'
        ]);
        $vehiculos = Vehiculo::where('id', '=', $id)->get()->first();
        $vehiculos->id_brand = $request->id_brand;
        $vehiculos->id_model = $request->id_model;
        $vehiculos->year = $request->year;
        $vehiculos->placa = $request->placa;
        $vehiculos->color = $request->color;
        $vehiculos->save();
        return response()->json(['success' => true]);
    }

    // función para eliminar logicamente un registro
    public function destroy($id){
        $vehiculos = Vehiculo::where('id', '=', $id)->get()->first();
        $vehiculos->status = '0';
        $vehiculos->save();
        return response()->json(['success' => true]);
    }
}
