@extends('layouts.index')

@section('content')
    <div class="">
        <form action="" id="formModelo" method="post">
            @csrf   
            <div class="form-group mb-3 mt-5">
                <div class="form-group mb-3">
                    <label for="">Nombre:</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="escribe una nombre" autofocus aria-describedby="helpId" required>
                </div>
                <div class="form-group mb-3">
                    <label for="">Descripcion:</label>
                    <input type="text" name="description" id="description" class="form-control" placeholder="escribe una descripcion" aria-describedby="helpId" required>
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="">Marca:</label>
                <select class="form-control" name="id_brand" id="id_brand" required>
                    <option>Seleccione una marca</option>
                    @foreach ($marcas as $marca)
                        <option value="{{$marca->id}}">{{$marca->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="float-right">
                <button class="btn btn-success" id="btnRegistrarModelo">Registrar</button>
                <a class="btn btn-warning" href="{{route('modelos.index')}}" id="btnRegistrarVehiculo">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@section('javascript')

    <!-- Petición ajax para guardar un registro en la base de datos -->
    <script>
        $('#btnRegistrarModelo').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let name  = $('#name').val();
            let description = $('#description').val();
            let _token = $('input[name=_token]').val();
            let id_brand = $('#id_brand').val();
            // Petición ajax
            $.ajax({
                type: "POST",
                url: "{{route('modelos.store')}}",
                data: {
                    name: name,
                    description: description,
                    id_brand: id_brand,
                    _token: _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Registro exitoso', 'Nuevo registro');
                        window.location.href = '/modelos'
                    }, 1000);
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });
        });
    </script>
@endsection


