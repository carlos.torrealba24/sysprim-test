@extends('layouts.index')

@section('content')

    <div class="container">
        <div class="row mb-5 mt-5">
            <div class="col-md-6" style="display:flex;">
                <h3 class="text-secondary">Modelos</h3>
                <a href="" class="btn btn-primary" style="margin-left: 30px;" id="btnNuevaModelo">Nuevo modelo</a>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table cell-border" id="table-generic">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">NOMBRE</th>
                    <th class="text-center">DESCRIPCIÓN</th>
                    <th class="text-center">MARCA</th>
                    <th class="text-center">ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($modelos as $modelo)
                    <tr id="rowId-{{$modelo->id}}">
                        <td class="text-center">
                            {{$modelo->id}}
                        </td>
                        <td class="text-center">
                            {{$modelo->name}}
                        </td>
                        <td class="text-center">
                            {{$modelo->description}}
                        </td>
                        <td class="text-center">
                            {{$modelo->marca->name}}
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{route('modelos.edit',$modelo->id)}}" id="editBtn" class="btn btn-warning" title="Modificar">Editar</a>
                                <form action="" style="display: inline" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button id="btnDelete" valor="{{$modelo->id}}" class="btn btn-danger" title="Eliminar"> Eliminar </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="col-md-12">
            <form action="" method="get">
                <button class="btn btn-warning" type="submit">Back</button>
            </form>
        </div>
    </div>

@endsection

@section('javascript')

    <!-- Petición ajax para llamar a la vista create -->
    <script>
        $('#btnNuevaModelo').on('click', function(e){
            // Petición ajax
            $.ajax({
                type: "GET",
                url: "{{route('modelos.create')}}",
                success: function() { 
                    window.location.href = '/modelos/create'
                },
                error: function(xhr, ajaxOptions, thrownerror) { }
            })
        });
    </script>
    <script>
        // Ajax para eliminar un registro
        $(document).on('click', '#btnDelete', function(e){
            if(confirm('Está seguro que quiere eliminar este registro?')){
                e.preventDefault();
                let id = $(this).attr('valor');
                let _token = $('input[name=_token]').val();
                $.ajax({
                    type: "DELETE",
                    url: `modelos/${id}`,
                    data: {
                        id: id,
                        _token: _token
                    },
                    success:function(response){
                        toastr.success('Registro Eliminado', 'Eliminar', 3000);
                        $('#rowId-'+id).remove();
                    },
                    error: function (err) {
                        if(err.status == 422){
                            $.each(err.responseJSON.errors, function (i,error) {
                                toastr.success('error en eliminacion', +error[0], 3000);
                            })
                        }
                    }
                });
            }
        });
    </script>
@endsection