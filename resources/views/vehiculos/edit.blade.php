@extends('layouts.index')

@section('content')
    <div class="">
        <form action="" id="formVehiculo" method="post">
            @csrf   

            <div class="form-group mb-3 mt-5">
                <label for="">Marca:</label>
                <select class="form-control" name="id_brand" id="id_brand" autofocus required>
                    <option>Seleccione una marca</option>
                    @foreach ($marcas as $marca)
                        <option value="{{$marca->id}}" {{$vehiculos->id_brand == $marca->id ? 'selected' : ''}}>{{$marca->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group mb-3">
                <label for="">Modelo:</label>
                <select class="form-control" name="id_model" id="id_model" required>
                    <option>Seleccione un modelo</option>
                    @foreach ($modelos as $model)
                        <option value="{{$model->id}}" {{$vehiculos->id_model == $model->id ? 'selected' : ''}}>{{$model->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group mb-3">
                <div class="form-group mb-3">
                    <label for="">Año:</label>
                    <input type="number" name="year" id="year" class="form-control" placeholder="escribe una año" aria-describedby="helpId" value="{{$vehiculos->year}}" required>
                </div>
                <div class="form-group mb-3">
                    <label for="">Placa:</label>
                    <input type="text" name="placa" id="placa" class="form-control" placeholder="escribe una placa" aria-describedby="helpId" value="{{$vehiculos->placa}}" required>
                </div>
            </div>

            <div class="form-group mb-3">
                <div class="form-group mb-3">
                    <label for="">Color:</label>
                    <input type="text" name="color" id="color" class="form-control" placeholder="escribe un color" aria-describedby="helpId" value="{{$vehiculos->color}}" required>
                </div>
            </div>
            <div class="float-right">
                <button class="btn btn-success" id="btnRegistrarVehiculo">Registrar</button>
                <a class="btn btn-warning" href="{{route('vehiculos.index')}}" id="btnRegistrarVehiculo">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@section('javascript')

    <!-- Petición ajax para modificar un registro en la base de datos -->
    <script>
        $('#btnRegistrarVehiculo').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let id_brand  = $('#id_brand').val();
            let id_model = $('#id_model').val();
            let year = $('#year').val();
            let plate = $('#placa').val();
            let placa = plate.toUpperCase();
            let color = $('#color').val();
            let _token = $('input[name=_token]').val();
            // Petición ajax
            $.ajax({
                type: "PUT",
                url: "{{route('vehiculos.update', $vehiculos->id)}}",
                data: {
                    id_brand: id_brand,
                    id_model: id_model,
                    year: year,
                    placa: placa,
                    color: color,
                    _token: _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Modificación exitosa', 'Modificar registro');
                        window.location.href = '/vehiculos'
                    }, 1000);
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });
        });
    </script>
@endsection


