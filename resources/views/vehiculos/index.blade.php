@extends('layouts.index')

@section('content')

    <div class="container">
        <div class="row mb-5 mt-5">
            <div class="col-md-6" style="display:flex;">
                <h3 class="text-secondary">Vehiculos</h3>
                <a href="" class="btn btn-primary" style="margin-left: 30px;" id="btnNuevoVehiculo">Nuevo vehiculo</a>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table cell-border" id="table-generic">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">MARCA</th>
                    <th class="text-center">MODELO</th>
                    <th class="text-center">AÑO</th>
                    <th class="text-center">PLACA</th>
                    <th class="text-center">COLOR</th>
                    <th class="text-center">FECHA INGRESO</th>
                    <th class="text-center">ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vehiculos as $auto)
                    <tr id="rowId-{{$auto->id}}">
                        <td class="text-center">
                            {{$auto->id}}
                        </td>
                        <td class="text-center">
                            {{$auto->marca->name}}
                        </td>
                        <td class="text-center">
                            {{$auto->modelo->name}}
                        </td>
                        <td class="text-center">
                            {{$auto->year}}
                        </td>
                        <td class="text-center">
                            {{$auto->placa}}
                        </td>
                        <td class="text-center">
                            {{$auto->color}}
                        </td>
                        <td class="text-center">
                            {{$auto->created_at->format('d/m/Y')}}
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{route('vehiculos.edit',$auto->id)}}" id="editBtn" class="btn btn-warning" title="Modificar">Editar</a>
                                <form action="{{route('vehiculos.destroy', $auto->id)}}" style="display: inline" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button id="btnDelete" valor="{{$auto->id}}" class="btn btn-danger" title="Eliminar"> Eliminar </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('javascript')

    <!-- Petición ajax para llamar a la vista create -->
    <script>
        $('#btnNuevoVehiculo').on('click', function(e){
            // Petición ajax
            $.ajax({
                type: "GET",
                url: "{{route('vehiculos.create')}}",
                success: function() { 
                    window.location.href = '/vehiculos/create'
                },
                error: function(xhr, ajaxOptions, thrownerror) { }
            })
        });
    </script>

    <script>
        // Ajax para eliminar un registro
        $(document).on('click', '#btnDelete', function(e){
            if(confirm('Está seguro que quiere eliminar este registro?')){
                e.preventDefault();
                let id = $(this).attr('valor');
                let _token = $('input[name=_token]').val();
                $.ajax({
                    type: "DELETE",
                    url: `vehiculos/${id}`,
                    data: {
                        id: id,
                        _token: _token
                    },
                    success:function(response){
                        toastr.success('Registro Eliminado', 'Eliminar', 3000);
                        $('#rowId-'+id).remove();
                    },
                    error: function (err) {
                        if(err.status == 422){
                            $.each(err.responseJSON.errors, function (i,error) {
                                toastr.success('error en eliminacion', +error[0], 3000);
                            })
                        }
                    }
                });
            }
        });
    </script>
@endsection