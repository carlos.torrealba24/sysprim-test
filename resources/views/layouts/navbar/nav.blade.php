<nav class="navbar navbar-expand-lg navbar-dark  bg-primary">
    <a class="navbar-brand" style="margin-left: 10px;" href="">Sysprim C.A</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @if(auth()->check())
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('marcas.index')}}">Marcas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('modelos.index')}}">Modelos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('vehiculos.index')}}">Vehiculos</a>
                </li>
            </ul>
            <ul class="navbar-nav" style="margin-left: auto">
                <li class="nav-item">
                    <p class="nav-link">{{auth()->user()->name}}</p>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('auth.destroy')}}">Salir</a>
                </li>
            </ul>
            
            @else
                <ul class="navbar-nav" style="margin-left: auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('auth.register')}}">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('auth.login')}}">Entrar</a>
                    </li>
                </ul>
        @endif
        
    </div>
</nav>

