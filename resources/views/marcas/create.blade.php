@extends('layouts.index')

@section('content')
    <div class="">
        <form action="" id="formMarca" method="post">
            @csrf   
            <div class="form-group mb-3 mt-5">
                <div class="form-group mb-3">
                    <label for="">Nombre:</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="escribe una nombre" aria-describedby="helpId" required>
                </div>
                <div class="form-group mb-3">
                    <label for="">Descripcion:</label>
                    <input type="text" name="description" id="description" class="form-control" placeholder="escribe una descripcion" aria-describedby="helpId" required>
                </div>
            </div>
            
            <div class="float-right">
                <button class="btn btn-success" id="btnRegistrarMarca">Registrar</button>
                <a class="btn btn-warning" href="{{route('marcas.index')}}" id="btnRegistrarVehiculo">Atras</a>
            </div>
        </form>
    </div> 
@endsection

@section('javascript')

    <!-- Petición ajax para guardar un registro en la base de datos -->
    <script>
        $('#btnRegistrarMarca').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let name  = $('#name').val();
            let description = $('#description').val();
            let _token = $('input[name=_token]').val();
            // Petición ajax
            $.ajax({
                type: "POST",
                url: "{{route('marcas.store')}}",
                data: {
                    name: name,
                    description: description,
                    _token: _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Registro exitoso', 'Nuevo registro');
                        window.location.href = '/marcas'
                    }, 1000);
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });
        });
    </script>
@endsection


