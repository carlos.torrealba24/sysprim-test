@extends('layouts.index')

@section('content')
    <div class="container">
        <div class="">
            <form action="" id="formMarca" method="post">
                @csrf   
                @method('PUT')
                <div class="form-group mb-3 mt-5">
                    <div class="form-group mb-3">
                        <label for="">Nombre:</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="escribe una nombre" autofocus aria-describedby="helpId" value="{{$marcas->name}}" required>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Descripcion:</label>
                        <input type="text" name="description" id="description" class="form-control" placeholder="escribe una descripcion" aria-describedby="helpId" value="{{$marcas->description}}" required>
                    </div>
                </div>
                
                <div class="float-right">
                    <button class="btn btn-success" id="btnEditarMarca">Editar</button>
                    <a class="btn btn-warning" href="{{route('marcas.index')}}" id="btnRegistrarVehiculo">Atras</a>
                </div>
            </form>
        </div>
    </div>    
@endsection

@section('javascript')

    <!-- Petición ajax para modificar un registro en la base de datos -->
    <script>
        $('#btnEditarMarca').on('click', function(e){
            e.preventDefault();
            // obtener valores de los inputs
            let name  = $('#name').val();
            let description = $('#description').val();
            let _token = $('input[name=_token]').val();
            // Petición ajax
            $.ajax({
                type: "PUT",
                url: "{{route('marcas.update', $marcas->id)}}",
                data: {
                    name: name,
                    description: description,
                    _token: _token
                },
                success:function(response){
                    setTimeout(() => {
                        toastr.success('Modificación exitosa', 'Modificar registro');
                        window.location.href = '/marcas'
                    }, 1000);
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });
        });
    </script>
@endsection